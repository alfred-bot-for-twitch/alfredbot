#!/bin/sh

#pyinstaller -F --windowed --add-data "/usr/local/lib/python3.7/site-packages/sc2:sc2" --add-data "/usr/local/lib/python3.7/site-packages/numpy:numpy" --icon alfred-pennyworth-young-justice-8.29.ico alfred.py
pyinstaller -y --clean index.spec --onefile
#pushd dist
#hdiutil create ./Alfred.dmg -srcfolder alfred.app -ov
#popd
cp dist/alfred ../eliza/bot/