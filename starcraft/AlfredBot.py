import sc2
from sc2.constants import UnitTypeId
from sc2.unit import Unit

from starcraft.constans import Commands


class AlfredBot(sc2.BotAI):

    def __init__(self, queue, user):
        super().__init__()
        self._commands_queue = queue
        self._commands_processed = 0
        self._user = user
        self._create_new_game_instance_in_hasura()

    async def on_step(self, iteration):
        await self.distribute_workers()
        while not self._commands_queue.empty() and self._commands_processed <= Commands.BATCH_SIZE_POLICY:
            command_to_process = await self._commands_queue.get()
            await self._process_command(command_to_process)
            self._commands_processed += 1
        self._commands_processed = 0

    async def build_workers(self):
        for nexus in self.units(UnitTypeId.NEXUS).ready.idle:
            if self.can_afford(UnitTypeId.PROBE):
                await self.do(nexus.train(UnitTypeId.PROBE))

    async def _send_fail_message(self, command: str):
        structure = command.split()[1]
        await self.chat_send(structure + ": cannot afford to build it")

    async def build_pylons(self, command: str):
        result = await self.build_structures(UnitTypeId.PYLON, command)
        return result

    async def build_assimilators(self, command: str):
        nexus = self.units(UnitTypeId.NEXUS).ready.random
        vaspene = self.state.vespene_geyser.closer_than(25.0, nexus).random
        worker = self.select_build_worker(vaspene.position)

        await self.build_structures_by_workers(UnitTypeId.ASSIMILATOR, worker, vaspene, command)

    async def build_cybernetic_core(self, command: str):
        await self.build_structures(UnitTypeId.CYBERNETICSCORE, command)

    async def build_gateway(self, command: str):
        await self.build_structures(UnitTypeId.GATEWAY, command)

    async def build_photon(self, command: str):
        await self.build_structures(UnitTypeId.PHOTONCANNON, command)

    async def build_twilight_council(self, command: str):
        await self.build_structures(UnitTypeId.TWILIGHTCOUNCIL, command)

    async def build_forge(self, command: str):
        await self.build_structures(UnitTypeId.FORGE, command)

    async def build_structures(self, structure: UnitTypeId, command: str):
        nexuses_selected = self.units(UnitTypeId.NEXUS).selected
        nexus = self.units(UnitTypeId.NEXUS).random
        try:
            if nexuses_selected.exists:
                nexus = nexuses_selected.random
        except AttributeError:
            pass
        if self.can_afford(structure):
            await self.build(structure, near=nexus)
            return
        await self._send_fail_message(command)

    async def build_structures_by_workers(self, structure: UnitTypeId, worker: Unit, base: Unit, command: str):
        if self.can_afford(structure):
            await self.do(worker.build(structure, base))
        await self._send_fail_message(command)

    async def _process_command(self, command):

        try:
            if command == Commands.BUILD_WORKER:
                await self.build_workers(command)
                return

            if command == Commands.BUILD_PYLONS:
                await self.build_pylons(command)
                return

            if command == Commands.BUILD_CYBERNETICS_CORE:
                await self.build_cybernetic_core(command)
                return

            if command == Commands.BUILD_PHOTON_CANNON:
                await self.build_photon(command)
                return

            if command == Commands.BUILD_TWILIGHT_COUNCIL:
                await self.build_twilight_council(command)
                return

            if command == Commands.BUILD_ASSIMILATORS:
                await self.build_assimilators(command)
                return

            if command == Commands.BUILD_FORGE:
                await self.build_forge(command)
                return

            if command == Commands.BUILD_GATEWAY:
                await self.build_gateway(command)
                return

        except AssertionError:
            await self._send_fail_message(command)
        except AttributeError:
            await self._send_fail_message(command)

    def _create_new_game_instance_in_hasura(self):
        pass
