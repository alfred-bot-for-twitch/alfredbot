class Commands:
    BUILD_GATEWAY = "Build Gateway"
    BUILD_WORKER = "Build Workers"
    BUILD_PYLONS = "Build Pylons"
    BUILD_TWILIGHT_COUNCIL = "Build Twilight Council"
    BUILD_PHOTON_CANNON = "Build Photon Cannon"
    BUILD_CYBERNETICS_CORE = "Build Cybernetics Core"
    BUILD_ASSIMILATORS = "Build Assimilator"
    BUILD_FORGE = "Build Forge"
    BATCH_SIZE_POLICY = 10
