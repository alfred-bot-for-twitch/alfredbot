import zerorpc
from twitch.AlfredBotServer import AlfredRpcController


def main():
    server = zerorpc.Server(AlfredRpcController())
    server.bind('tcp://0.0.0.0:4242')
    server.run()


if __name__ == '__main__':
    main()
