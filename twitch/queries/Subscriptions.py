import asyncio
import datetime

import pytz


class CommandsSubscription:

    def __init__(self, queue, graphql_client, user):
        self._commands_queue = queue
        self._last_command_chkpnt = None
        self._graphql_client = graphql_client
        self._user = user
        asyncio.create_task(self._start_commands_subscription())

    def get_gql_variables(self, channel_id):
        return {
            "channel_id": channel_id
        }

    def get_commands_query(self):
        return '''
            subscription ($channelId: String!, $created_at: timestamptz!) {
              USER_COMMANDS(where: {channel_id: {_eq: $channelId}, created_at: {_gte: $created_at}}, order_by: {created_at: desc}) {
                channel_id
                command
                command_id
                created_at
              }
            }
        '''

    def _get_delete_commands(self):
        return '''
            mutation {
                delete_USER_COMMANDS(where: {channel_id: {_eq: $channel_id}, _and: {created_at: {_lte: "$created_at"}}}}) {
                affected_rows
              }
            }
        '''

    async def _batch_delete_past_10_min(self):
        gql_variables = {
            "channel_id": self._user['channelId'],
            "command_id": self._last_command_chkpnt
        }
        await asyncio.sleep(10)
        await self._graphql_client.post(self.get_commands_query(), gql_variables)

    async def commands_subscription_handler(self, commands_payload):
        commands = commands_payload['data']['USER_COMMANDS']
        # if self._last_command_chkpnt is not None:
        # commands = list(
        #     filter(lambda command: command['command_id'] > self._last_command_chkpnt['command_id'], commands))

        for command in commands:
            if self._last_command_chkpnt is None:
                await self._commands_queue.put(command['command'])

            if self._last_command_chkpnt is not None:
                if datetime.datetime.fromisoformat(
                        command['created_at']) > datetime.datetime.fromisoformat(
                    self._last_command_chkpnt['created_at']):
                    await self._commands_queue.put(command['command'])
            self._last_command_chkpnt = command

    async def _start_commands_subscription(self):
        app_start_time = pytz.utc.localize(datetime.datetime.utcnow()).isoformat()
        gql_variables = {
            "channelId": self._user['channelId'],
            "created_at": app_start_time
        }
        await self._graphql_client.subscribe(self.get_commands_query(),
                                             gql_variables,
                                             None,
                                             self.commands_subscription_handler)
