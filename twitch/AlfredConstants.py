class HasuraConstants:
    # Client -> Server
    GQL_CONNECTION_INIT = 'connection_init'
    GQL_START = 'start'
    GQL_DATA = 'data'
    GQL_ERROR = 'error'
    GQL_COMPLETE = 'complete'
    GQL_STOP = 'stop'
    GQL_CONNECTION_KEEP_ALIVE = 'ka'
    GQL_CONNECTION_ACK = 'connection_ack'
