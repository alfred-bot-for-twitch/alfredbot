import asyncio
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer

from starcraft.AlfredBot import AlfredBot
from twitch.AlfredHasuraClient import AlfredHasuraClient
from twitch.queries.Subscriptions import CommandsSubscription
import logging

logger = logging.getLogger(__name__)


async def establish_connection_to_hasura(commands_queue, user):
    async with AlfredHasuraClient(user=user) as ws:
        CommandsSubscription(commands_queue, ws, user)
        await ws.handle_incoming_message()


def start_starcraft_2_game(commands_queue, user):
    run_game(maps.get("CyberForestLE"), [
        Bot(Race.Protoss, AlfredBot(commands_queue, user)),
        Computer(Race.Protoss, Difficulty.Medium)
    ], realtime=True)


def start_bot_and_game(user):
    commands_queue = asyncio.Queue()
    #asyncio.get_event_loop().create_task(establish_connection_to_hasura(commands_queue, user))
    asyncio.run(establish_connection_to_hasura(commands_queue, user))
    #start_starcraft_2_game(commands_queue, user)


class AlfredRpcController(object):

    def launch_game(self, user):
        # logger.info("Start game request received from a RPC client.")
        start_bot_and_game(user)
