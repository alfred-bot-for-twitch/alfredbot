"""
A dead simple GraphQL client that works over Websocket as the transport
protocol, instead of HTTP.
This follows the Apollo protocol.
https://github.com/apollographql/subscriptions-transport-ws/blob/master/PROTOCOL.md
"""

import asyncio
import json
import logging
import random
import signal
import string
import tempfile
from typing import Optional

import aiohttp
from aiohttp import WSMsgType, WSMessage
from sc2.sc2process import kill_switch

from twitch.AlfredConstants import HasuraConstants

logger = logging.getLogger(__name__)


class AlfredHasuraClient:
    def __init__(
            self, host: str = "127.0.0.1", port: Optional[int] = 8009, render: bool = False, user=None
    ) -> None:
        assert isinstance(host, str)
        assert isinstance(port, int) or port is None

        self._render = render
        self._host = host
        self._port = port
        self._tmp_dir = tempfile.mkdtemp(prefix="SC2_")
        self._process = None
        self._session = None
        self._ws = None
        self._operations = {}
        self._listening = False
        self._user = user

    async def __aenter__(self):
        kill_switch.add(self)

        async def signal_handler(*args):
            await kill_switch.kill_all()

        signal.signal(signal.SIGINT, signal_handler)

        try:
            self._ws = await self._connect()
            await self._gql_conn_init()
            self._listening = True
        except Exception:
            await self._close_connection()
            await self._clean()
            raise

        return self

    def _get_headers(self):
        return {'authorization': 'Bearer' + ' ' + self._user['hasuraAccessToken']}

    async def __aexit__(self, *args):
        self._listening = False
        await self._close_connection()

    async def _gql_conn_init(self, headers=None):
        payload = {
            'type': HasuraConstants.GQL_CONNECTION_INIT,
            'payload': {'headers': headers}
        }
        await self._ws.send_json(payload)

    async def _gql_start(self, payload):
        _id = self.gen_id()
        frame = {'id': _id, 'type': HasuraConstants.GQL_START, 'payload': payload}
        try:
            await self._ws.send_json(frame)
        except Exception:
            print(Exception)
        return _id

    async def _gql_stop(self, _id):
        payload = {'id': _id, 'type': HasuraConstants.GQL_STOP}
        await self._ws.send_json(payload)
        return await self._ws.receive_json()

    async def post(self, query, variables=None, headers=None):
        payload = {
            'headers': headers,
            'query': query,
            'variables': variables
        }
        _id = await self._gql_start(payload)
        return _id

    async def subscribe(self, query, variables=None, headers=None, callback=None):
        payload = {
            'headers': self._get_headers(),
            'query': query,
            'variables': variables
        }
        _id = await self._gql_start(payload)
        self._operations[_id] = callback
        return _id

    async def handle_incoming_message(self):
        while self._listening:
            result = await self._ws.receive()
            print(result)
            self._parse_incoming_message(result)

    def _parse_incoming_message(self, message):
        message = self._detect_message_type(message)
        print(message)
        if HasuraConstants.GQL_CONNECTION_KEEP_ALIVE in message.data:
            return
        if HasuraConstants.GQL_CONNECTION_INIT in message.data:
            return

        if HasuraConstants.GQL_CONNECTION_ACK in message.data:
            return

        if message is not None:
            parsed_gql_response = message.json()
            print(parsed_gql_response)
            payload = parsed_gql_response['payload']
            op_id = parsed_gql_response['id']

            if parsed_gql_response['type'] == HasuraConstants.GQL_DATA:
                if 'errors' in parsed_gql_response:
                    logger.info(parsed_gql_response['erros'])
                    return
                asyncio.create_task(self._operations[op_id](payload))
                return

            if parsed_gql_response['type'] == HasuraConstants.GQL_COMPLETE:
                self._operations[op_id](None)
                del self._operations[op_id]
                return

    def _detect_message_type(self, message):
        if message.type == WSMsgType.TEXT:
            return message

        if message.type == WSMsgType.CLOSED or WSMsgType.CLOSE:
            return None

        if message.type == WSMsgType.ERROR:
            logger.debug(message)
            return None

        if isinstance(message, WSMessage):
            return None

        return json.loads(message)

    async def _unsubscribe(self, subscription_id):
        await self._gql_stop(subscription_id)
        del self._operations[subscription_id]

    @property
    def ws_url(self):
        return f"ws://{self._host}:{self._port}/v1/graphql"

    async def _connect(self):
        for i in range(60):

            await asyncio.sleep(1)
            try:
                self._session = aiohttp.ClientSession()
                ws = await self._session.ws_connect(self.ws_url, timeout=120, headers=self._get_headers())
                logger.info("Websocket connection ready")
                return ws
            except aiohttp.client_exceptions.ClientConnectorError:
                await self._session.close()
                if i > 15:
                    logger.debug("Connection refused (startup not complete (yet))")

        logger.debug("Websocket connection to Hasura timed out")
        raise TimeoutError("Websocket")

    async def _close_connection(self):
        logger.info("Closing connection...")

        if self._ws is not None:
            await self._ws.close()

        if self._session is not None:
            await self._session.close()

        if self._listening:
            self._listening = False

    def _clean(self):
        logger.info("Stopping the subscription loop")
        self._listening = False

    # generate random alphanumeric id
    def gen_id(self, size=6, chars=string.ascii_letters + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))
