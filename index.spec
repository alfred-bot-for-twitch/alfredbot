# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['alfred.py'],
             pathex=['/Users/bk670992/Hacks/TwitchExtensions/SC2TwitchBot'],
             binaries=[],
             datas=[('/usr/local/lib/python3.7/site-packages/sc2', 'sc2'), ('/usr/local/lib/python3.7/site-packages/numpy', 'numpy'), ('/usr/local/lib/python3.7/site-packages/zerorpc', 'zerorpc')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='alfred',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True , icon='alfred-pennyworth-young-justice-8.29.ico')
